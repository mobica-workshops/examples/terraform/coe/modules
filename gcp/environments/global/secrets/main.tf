// https://github.com/getsops/sops#encrypting-using-gcp-kms
// https://blog.gruntwork.io/a-comprehensive-guide-to-managing-secrets-in-your-terraform-code-1d586955ace1
resource "google_kms_key_ring" "sops" {
  name     = "sops"
  location = "europe"
}

resource "google_kms_crypto_key" "sops" {
  name     = "sops-key"
  key_ring = google_kms_key_ring.sops.id
}