variable "project_short_name" {
  type = string
}
variable "gcp_cloud_name" {
  type = string
}
variable "gcp_cloud_dns_name" {
  type = string
}
variable "management_project_id" {
  type = string
}