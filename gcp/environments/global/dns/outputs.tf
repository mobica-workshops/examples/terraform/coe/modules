output "project_cloud_name" {
  value = google_dns_managed_zone.project.name
}

output "project_cloud_dns_name" {
  value = google_dns_managed_zone.project.dns_name
}