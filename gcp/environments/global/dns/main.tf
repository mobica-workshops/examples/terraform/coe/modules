data "google_project" "project" {}

resource "google_dns_managed_zone" "project" {
  name        = "${var.project_short_name}-${var.gcp_cloud_name}"
  dns_name    = "${var.project_short_name}.${var.gcp_cloud_dns_name}"
  description = "CoE DevOps/Cloud GCP - ${var.project_short_name} - zone"
  project     = data.google_project.project.project_id
}

resource "google_dns_record_set" "gcp_cloud_mobica_com_NS" {
  name         = google_dns_managed_zone.project.dns_name
  managed_zone = var.gcp_cloud_name
  type         = "NS"
  ttl          = 3600

  rrdatas = google_dns_managed_zone.project.name_servers
  project = var.management_project_id
}
