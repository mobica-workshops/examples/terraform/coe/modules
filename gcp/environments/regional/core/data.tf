data "google_project" "project" {}

data "google_client_config" "this" {
  provider = google
}

data "google_container_engine_versions" "gke_version" {
  version_prefix = "1.27."
}