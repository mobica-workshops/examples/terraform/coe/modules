variable "chart_version" {
  default     = "13.17.1"
  description = "Helm chart version"
}

variable "namespace" {
  type        = string
  description = "namespace which will be used by the mongodb cluster, for example: workshops-staging"
}

variable "rootPassword" {
  type        = string
  description = "MongoDB cluster root user password"
}

variable "bookAdminPassword" {
  type        = string
  description = "MongoDB cluster book-admin user password"
}
