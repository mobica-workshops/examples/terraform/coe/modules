resource "helm_release" "gitlab_runner" {
  name       = "mongodb-cluster"
  repository = "oci://registry-1.docker.io/bitnamicharts/"
  chart      = "mongodb"
  version    = var.chart_version
  namespace  = var.namespace

  values = [
    templatefile("${path.module}/mongodb-values.yaml.tmpl", {
      rootPassword      = var.rootPassword
      bookAdminPassword = var.bookAdminPassword
    })
  ]
}
