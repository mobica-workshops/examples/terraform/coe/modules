resource "kubernetes_namespace_v1" "gitlab_runner" {
  metadata {
    annotations = {
      environment = var.namespace
    }

    labels = {
      environment = var.namespace
    }

    name = var.namespace
  }
}