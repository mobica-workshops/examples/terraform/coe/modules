resource "helm_release" "gitlab_runner" {
  name       = "gitlab-runner"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"
  version    = var.chart_version
  namespace  = kubernetes_namespace_v1.gitlab_runner.metadata[0].name

  values = [
    templatefile("${path.module}/gitlab-runner-values.yaml.tmpl", {
      token      = var.token
      replicas   = var.replicas
      concurrent = var.concurrent
    })
  ]
}
