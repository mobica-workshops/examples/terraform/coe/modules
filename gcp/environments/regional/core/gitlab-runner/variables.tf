variable "chart_version" {
  default     = "0.58.1"
  description = "Helm chart version"
}

variable "namespace" {
  type        = string
  description = "namespace which will be used by the gitlab runner, for example: gitlab-runner"
}

variable "token" {
  type        = string
  description = "Gitlab token required by runner to work"
}

variable "replicas" {
  type    = number
  default = 1
}

variable "concurrent" {
  type    = number
  default = 2
}