resource "google_container_cluster" "primary" {
  name     = "${data.google_project.project.project_id}-gke"
  location = data.google_client_config.this.region

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name

  monitoring_config {
    enable_components = ["SYSTEM_COMPONENTS"]
    managed_prometheus {
      enabled = false
    }
  }

  maintenance_policy {
    daily_maintenance_window {
      start_time = "04:00"
    }
  }
}

resource "google_container_node_pool" "primary_nodes" {
  name     = google_container_cluster.primary.name
  cluster  = google_container_cluster.primary.name
  location = data.google_client_config.this.region

  version    = data.google_container_engine_versions.gke_version.release_channel_latest_version["STABLE"]
  node_count = var.gke_num_nodes

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = data.google_project.project.project_id
    }

    # preemptible  = true
    machine_type = "n1-standard-2"
    tags         = ["gke-node", "${data.google_project.project.project_id}-gke"]
    metadata     = {
      disable-legacy-endpoints = "true"
    }
  }
}
