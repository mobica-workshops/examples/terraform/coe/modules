variable "service_account_name" {
  type        = string
  description = "Service account name used to deploy to the set namespace, for example: gitlab-ci"
}

variable "namespace" {
  type        = string
  description = "namespace which will be used for the deployment, for example: staging"
}

variable "kubernetes_cluster_name" {
  type = string
}

variable "kubernetes_cluster_host" {
  type = string
}

variable "kubernetes_cluster_ca_certificate" {
  type = string
}