resource "kubernetes_namespace_v1" "deployment" {
  metadata {
    annotations = {
      environment = var.namespace
    }

    labels = {
      environment = var.namespace
    }

    name = var.namespace
  }
}
