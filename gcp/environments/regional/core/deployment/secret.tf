resource "kubernetes_secret_v1" "deployment" {
  metadata {
    name = var.service_account_name
    namespace = kubernetes_namespace_v1.deployment.metadata[0].name
    annotations = {
      "kubernetes.io/service-account.name" = kubernetes_service_account_v1.deployment.metadata[0].name
    }
  }
  type = "kubernetes.io/service-account-token"
}
