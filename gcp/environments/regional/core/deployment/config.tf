resource "local_sensitive_file" "staging" {
  content  = yamlencode({
    apiVersion: "v1",
    clusters: [
      {
        cluster: {
          certificate-authority-data: var.kubernetes_cluster_ca_certificate
          server: "https://${var.kubernetes_cluster_host}"
        }
        name: var.kubernetes_cluster_name
      }
    ],
    contexts: [
      {
        context: {
          cluster: var.kubernetes_cluster_name
          namespace: "staging"
          user: var.service_account_name
        }
        name: var.kubernetes_cluster_name
      }
    ],
    current-context: var.kubernetes_cluster_name
    kind: "Config"
    preferences: {}
    users: [
      {
        name: var.service_account_name
        user: {
          token: lookup(kubernetes_secret_v1.deployment.data, "token")  // kubernetes_token_request_v1.staging_gitlab_ci.token
        }
      }
    ]
  })
  filename = "${var.namespace}-config.yml"
}
