resource "kubernetes_service_account_v1" "deployment" {
  metadata {
    name = var.service_account_name
    namespace = kubernetes_namespace_v1.deployment.metadata[0].name
  }
  secret {
    name = var.service_account_name
  }
}

#resource "kubernetes_token_request_v1" "deployment" {
#  metadata {
#    name = kubernetes_service_account_v1.deployment.metadata.0.name
#  }
#  spec {
#    audiences = [
#      "api",
#      "vault",
#      "factors"
#    ]
#  }
#}
