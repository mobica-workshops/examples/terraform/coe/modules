variable "namespace" {
  type        = string
  default     = "cert-manager"
  description = "namespace which will be used by the gitlab runner, for example: cert-manager"
}

variable "issuer_name" {
  type    = string
  default = "letsencrypt-production"
}

variable "server" {
  type    = string
  default = "https://acme-v02.api.letsencrypt.org/directory"
}

variable "email" {
  type    = string
  default = "arkadiusz.tulodziecki@mobica.com"
}

variable "ingress_name" {
  type    = string
  default = "book-bff"
}