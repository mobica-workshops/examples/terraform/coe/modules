resource "kubernetes_manifest" "issuer" {
  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "Issuer"
    "metadata" = {
      "name"      = var.issuer_name
      "namespace" = var.namespace
    }
    "spec" = {
      "acme" = {
        "server" = var.server
        "email" = var.email
        "privateKeySecretRef" = {
          "name" = var.issuer_name
        }
        "solvers" = [
          {
            "http01" = {
              "ingress" = {
                "name" = var.ingress_name
              }
            }
          }
        ]
      }
    }
  }
}
