variable "chart_version" {
  default     = "v1.12.3"
  description = "Helm chart version"
}

variable "namespace" {
  type        = string
  default     = "cert-manager"
  description = "namespace which will be used by the gitlab runner, for example: cert-manager"
}
