resource "kubernetes_namespace_v1" "cert_manager" {
  metadata {
    annotations = {
      environment = var.namespace
    }

    labels = {
      environment = var.namespace
    }

    name = var.namespace
  }
}