## @section Global parameters
## Please, note that this will override the parameters, including dependencies, configured to use the global value
##
global:
  ## @param global.imageRegistry Global Docker image registry
  ##
  imageRegistry: ""
  ## @param global.imagePullSecrets Global Docker registry secret names as an array
  ## e.g.
  ## imagePullSecrets:
  ##   - myRegistryKeySecretName
  ##
  imagePullSecrets: []
  ## @param global.storageClass Global StorageClass for Persistent Volume(s)
  ##
  storageClass: ""
  postgresql:
    ## @param global.postgresql.auth.postgresPassword Password for the "postgres" admin user (overrides `auth.postgresPassword`)
    ## @param global.postgresql.auth.username Name for a custom user to create (overrides `auth.username`)
    ## @param global.postgresql.auth.password Password for the custom user to create (overrides `auth.password`)
    ## @param global.postgresql.auth.database Name for a custom database to create (overrides `auth.database`)
    ## @param global.postgresql.auth.existingSecret Name of existing secret to use for PostgreSQL credentials (overrides `auth.existingSecret`).
    ## @param global.postgresql.auth.secretKeys.adminPasswordKey Name of key in existing secret to use for PostgreSQL credentials (overrides `auth.secretKeys.adminPasswordKey`). Only used when `global.postgresql.auth.existingSecret` is set.
    ## @param global.postgresql.auth.secretKeys.userPasswordKey Name of key in existing secret to use for PostgreSQL credentials (overrides `auth.secretKeys.userPasswordKey`). Only used when `global.postgresql.auth.existingSecret` is set.
    ## @param global.postgresql.auth.secretKeys.replicationPasswordKey Name of key in existing secret to use for PostgreSQL credentials (overrides `auth.secretKeys.replicationPasswordKey`). Only used when `global.postgresql.auth.existingSecret` is set.
    ##
    auth:
      postgresPassword: "${rootPassword}"
      username: "book-list"
      password: "${bookListPassword}"
      database: "book-list"
      existingSecret: ""
      secretKeys:
        adminPasswordKey: ""
        userPasswordKey: ""
        replicationPasswordKey: ""
    ## @param global.postgresql.service.ports.postgresql PostgreSQL service port (overrides `service.ports.postgresql`)
    ##
    service:
      ports:
        postgresql: ""


## @section PostgreSQL Primary parameters
##
primary:
  ## @param primary.name Name of the primary database (eg primary, master, leader, ...)
  ##
  name: primary

  ## PostgreSQL Primary resource requests and limits
  ## ref: https://kubernetes.io/docs/user-guide/compute-resources/
  ## @param primary.resources.limits The resources limits for the PostgreSQL Primary containers
  ## @param primary.resources.requests.memory The requested memory for the PostgreSQL Primary containers
  ## @param primary.resources.requests.cpu The requested cpu for the PostgreSQL Primary containers
  ##
  resources:
    limits: {}
    requests:
      memory: 256Mi
      cpu: 250m

  ## PostgreSQL Primary persistence configuration
  ##
  persistence:
    ## @param primary.persistence.enabled Enable PostgreSQL Primary data persistence using PVC
    ##
    enabled: true
    ## @param primary.persistence.existingClaim Name of an existing PVC to use
    ##
    existingClaim: ""
    ## @param primary.persistence.mountPath The path the volume will be mounted at
    ## Note: useful when using custom PostgreSQL images
    ##
    mountPath: /bitnami/postgresql
    ## @param primary.persistence.subPath The subdirectory of the volume to mount to
    ## Useful in dev environments and one PV for multiple services
    ##
    subPath: ""
    ## @param primary.persistence.storageClass PVC Storage Class for PostgreSQL Primary data volume
    ## If defined, storageClassName: <storageClass>
    ## If set to "-", storageClassName: "", which disables dynamic provisioning
    ## If undefined (the default) or set to null, no storageClassName spec is
    ##   set, choosing the default provisioner.  (gp2 on AWS, standard on
    ##   GKE, AWS & OpenStack)
    ##
    storageClass: ""
    ## @param primary.persistence.accessModes PVC Access Mode for PostgreSQL volume
    ##
    accessModes:
      - ReadWriteOnce
    ## @param primary.persistence.size PVC Storage Request for PostgreSQL volume
    ##
    size: 8Gi
    ## @param primary.persistence.annotations Annotations for the PVC
    ##
    annotations: {}
    ## @param primary.persistence.labels Labels for the PVC
    ##
    labels: {}
    ## @param primary.persistence.selector Selector to match an existing Persistent Volume (this value is evaluated as a template)
    ## selector:
    ##   matchLabels:
    ##     app: my-app
    ##
    selector: {}
    ## @param primary.persistence.dataSource Custom PVC data source
    ##
    dataSource: {}
  ## PostgreSQL Primary Persistent Volume Claim Retention Policy
  ## ref: https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/#persistentvolumeclaim-retention
  ##
  persistentVolumeClaimRetentionPolicy:
    ## @param primary.persistentVolumeClaimRetentionPolicy.enabled Enable Persistent volume retention policy for Primary Statefulset
    ##
    enabled: false
    ## @param primary.persistentVolumeClaimRetentionPolicy.whenScaled Volume retention behavior when the replica count of the StatefulSet is reduced
    ##
    whenScaled: Retain
    ## @param primary.persistentVolumeClaimRetentionPolicy.whenDeleted Volume retention behavior that applies when the StatefulSet is deleted
    ##
    whenDeleted: Retain

## @section PostgreSQL read only replica parameters (only used when `architecture` is set to `replication`)
##
readReplicas:
  ## @param readReplicas.name Name of the read replicas database (eg secondary, slave, ...)
  ##
  name: read
  ## @param readReplicas.replicaCount Number of PostgreSQL read only replicas
  ##
  replicaCount: 1

  ## PostgreSQL read only resource requests and limits
  ## ref: https://kubernetes.io/docs/user-guide/compute-resources/
  ## @param readReplicas.resources.limits The resources limits for the PostgreSQL read only containers
  ## @param readReplicas.resources.requests.memory The requested memory for the PostgreSQL read only containers
  ## @param readReplicas.resources.requests.cpu The requested cpu for the PostgreSQL read only containers
  ##
  resources:
    limits: {}
    requests:
      memory: 256Mi
      cpu: 250m

  ## PostgreSQL read only persistence configuration
  ##
  persistence:
    ## @param readReplicas.persistence.enabled Enable PostgreSQL read only data persistence using PVC
    ##
    enabled: true
    ## @param readReplicas.persistence.existingClaim Name of an existing PVC to use
    ##
    existingClaim: ""
    ## @param readReplicas.persistence.mountPath The path the volume will be mounted at
    ## Note: useful when using custom PostgreSQL images
    ##
    mountPath: /bitnami/postgresql
    ## @param readReplicas.persistence.subPath The subdirectory of the volume to mount to
    ## Useful in dev environments and one PV for multiple services
    ##
    subPath: ""
    ## @param readReplicas.persistence.storageClass PVC Storage Class for PostgreSQL read only data volume
    ## If defined, storageClassName: <storageClass>
    ## If set to "-", storageClassName: "", which disables dynamic provisioning
    ## If undefined (the default) or set to null, no storageClassName spec is
    ##   set, choosing the default provisioner.  (gp2 on AWS, standard on
    ##   GKE, AWS & OpenStack)
    ##
    storageClass: ""
    ## @param readReplicas.persistence.accessModes PVC Access Mode for PostgreSQL volume
    ##
    accessModes:
      - ReadWriteOnce
    ## @param readReplicas.persistence.size PVC Storage Request for PostgreSQL volume
    ##
    size: 8Gi
    ## @param readReplicas.persistence.annotations Annotations for the PVC
    ##
    annotations: {}
    ## @param readReplicas.persistence.labels Labels for the PVC
    ##
    labels: {}
    ## @param readReplicas.persistence.selector Selector to match an existing Persistent Volume (this value is evaluated as a template)
    ## selector:
    ##   matchLabels:
    ##     app: my-app
    ##
    selector: {}
    ## @param readReplicas.persistence.dataSource Custom PVC data source
    ##
    dataSource: {}
  ## PostgreSQL Read only Persistent Volume Claim Retention Policy
  ## ref: https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/#persistentvolumeclaim-retention
  ##
  persistentVolumeClaimRetentionPolicy:
    ## @param readReplicas.persistentVolumeClaimRetentionPolicy.enabled Enable Persistent volume retention policy for read only Statefulset
    ##
    enabled: false
    ## @param readReplicas.persistentVolumeClaimRetentionPolicy.whenScaled Volume retention behavior when the replica count of the StatefulSet is reduced
    ##
    whenScaled: Retain
    ## @param readReplicas.persistentVolumeClaimRetentionPolicy.whenDeleted Volume retention behavior that applies when the StatefulSet is deleted
    ##
    whenDeleted: Retain
