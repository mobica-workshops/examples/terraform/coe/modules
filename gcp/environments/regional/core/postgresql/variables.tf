variable "chart_version" {
  default     = "12.10.0"
  description = "Helm chart version"
}

variable "namespace" {
  type        = string
  description = "namespace which will be used by the postgresql database, for example: workshops-staging"
}

variable "rootPassword" {
  type        = string
  description = "Postgresql database root user password"
}

variable "bookListPassword" {
  type        = string
  description = "Postgresql database book-list user password"
}
