resource "helm_release" "gitlab_runner" {
  name       = "postgres-database"
  repository = "oci://registry-1.docker.io/bitnamicharts/"
  chart      = "postgresql"
  version    = var.chart_version
  namespace  = var.namespace

  values = [
    templatefile("${path.module}/postgresql-values.yaml.tmpl", {
      rootPassword     = var.rootPassword
      bookListPassword = var.bookListPassword
    })
  ]
}
