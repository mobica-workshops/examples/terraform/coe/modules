data "google_project" "project" {
}

resource "random_id" "bucket_code" {
  byte_length = 8
}

resource "google_storage_bucket" "tfstate" {
  name          = "${data.google_project.project.project_id}-${random_id.bucket_code.hex}-tfstate"
  force_destroy = false
  location      = var.location
  storage_class = "STANDARD"
  versioning {
    enabled = true
  }
}
