variable "location" {
  type = string
}

variable "rg_name" {
  type        = string
  description = "Resource Group name"
}