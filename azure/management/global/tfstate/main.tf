locals {
  storage_account_name_prefix = substr(replace(var.rg_name, "-", ""), 0 , 12)
}

resource "azurerm_resource_group" "tfstate" {
  name     = var.rg_name
  location = var.location
}

resource "random_string" "tfstate" {
  length  = 5
  special = false
  upper   = false
}

resource "azurerm_storage_account" "tfstate" {
  account_replication_type = "LRS"
  account_tier             = "Standard"
  location                 = azurerm_resource_group.tfstate.location
  name                     = "${local.storage_account_name_prefix}${random_string.tfstate.result}tfstate"
  resource_group_name      = azurerm_resource_group.tfstate.name

  allow_nested_items_to_be_public = true

  tags = {
    environment = "mgmt"
  }
}

resource "azurerm_storage_container" "tfstate" {
  name                  = "${var.rg_name}-tfstate"
  storage_account_name  = azurerm_storage_account.tfstate.name
  container_access_type = "blob"
}