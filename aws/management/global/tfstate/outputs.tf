output "s3_bucket_name" {
  value = aws_s3_bucket.tfstate.bucket
}

output "kms_key_arn" {
  value = aws_kms_key.tfstate.arn
}

output "dynamodb_table_arn" {
  value = aws_dynamodb_table.tfstate.arn
}